﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JAlienInvasion.Controller;

namespace JAlienInvasion.Model
{
    interface IPlayer
    {
		void Draw();


		void Move(double deltaX, double deltaY);


		void Firing();
		void Hit(double damage);

		void MoveUp();



		void MoveDown();



		void MoveLeft();

		void MoveRight();

		void SetDirection(Direction d);
		void DeleteDirection(Direction d);

		//Metodo per effettuare i movimenti in base alle variabili di direzione impostate

	
	}
}

 
