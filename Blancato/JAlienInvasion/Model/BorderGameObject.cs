﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JAlienInvasion.Controller;

namespace JAlienInvasion.Model
{
    public class BorderGameObject : GameObject
    {
       
        public BorderGameObject(double width, double height, double positionX, double positionY) : base( width,  height,  positionX,  positionY)
		{
			
		}



		public void CheckMoveX(double deltaX)
		{
			double tmpX;
			tmpX = this.X  + ScreenManager.GetScaleX(deltaX);

			if (tmpX + this.Width <= ScreenManager.WidthScreen && tmpX >= 0)
			{
				this.X = (this.X + ScreenManager.GetScaleX(deltaX));
			}
			else
			{
				if (ScreenManager.WidthScreen - (tmpX + this.Width) < deltaX && tmpX >= 0)
				{
					this.X = ((int)ScreenManager.WidthScreen - this.Width);
				}
				else
				{
					if (tmpX < 0)
					{
						this.X = 0;
					}
				}
			}
		}

		public void CheckMoveY(double deltaY)
		{
			double tmpY;
			tmpY = this.Y + ScreenManager.GetScaleY(deltaY);

			if (tmpY + this.Height <= ScreenManager.HeightScreen && tmpY >= 0)
			{
				this.Y = tmpY;
			}
			else
			{
				
				if (ScreenManager.HeightScreen - (tmpY + this.Height) < ScreenManager.GetScaleY(deltaY) && tmpY >= 0)
				{
					this.Y = ScreenManager.HeightScreen - this.Height;
				}
				else
				{
					if (tmpY < 0)
					{
						this.Y = 0;
					}
				}
			}
		}
	}
}
