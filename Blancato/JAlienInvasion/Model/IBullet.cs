﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JAlienInvasion.Model
{
    public interface IBullet
    {
        bool UpdateMovement();
    }
}
