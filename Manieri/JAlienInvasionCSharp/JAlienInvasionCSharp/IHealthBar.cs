﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace JAlienInvasion.Model
{
    public interface IHealthBar
    {
		/// <returns> return health bar height
		///  </returns>
		double HealthHeight { get; }

		/// <summary>
		/// set health when not equal to maximum </summary>
		/// <param name="value"> decimal value used to set current health rectangle width
		///  </param>
		double CurrentHealth { set; }

		/// <summary>
		/// draw health bar rectangles at certain position </summary>
		/// <param name="gc"> graphic context used to draw on the window </param>
		/// <param name="x"> coordinate x where the rectangle has to be drew </param>
		/// <param name="y"> coordinate y where the rectangle has to be drew </param>
		void DrawRectangle(Graphics gc);
	}
}
