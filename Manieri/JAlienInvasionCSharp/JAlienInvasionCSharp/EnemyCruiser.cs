﻿using JAlienInvasion.Controller;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace JAlienInvasion.Model
{

	public class EnemyCruiser : EnemyImpl
    {
		private int stepMotion = 0;
		private int stepMovement = 0;
		private bool changeDir = false;

		public EnemyCruiser(double width, double height, double positionX, double positionY) : base(width, height, positionX, positionY)
		{

			try
			{
				this.Img = ScreenManager.GetImage("EnemySpaceShipCruiser.png");
			}
			catch (Exception)
			{
				Console.WriteLine("errore immagine");
			}
		}


		public override bool UpdateMovement()
        {
			if (stepMotion >= 15)
			{
				changeDir = !changeDir;
				stepMotion = 0;
			}
			else
			{
				stepMovement++;
			}
			if (stepMovement >= 5)
			{
				stepMotion++;
				stepMovement = 0;
				if (changeDir)
				{
					MoveDown();
					MoveRight();
					MoveLeft();
					MoveLeft();
					MoveUp();
					MoveDown();
				}
				else
				{
					MoveDown();
					MoveLeft();
					MoveRight();
					MoveRight();
					MoveUp();
					MoveDown();
				}
			}
			return this.CheckBottomBorder();

		}
	}
}
