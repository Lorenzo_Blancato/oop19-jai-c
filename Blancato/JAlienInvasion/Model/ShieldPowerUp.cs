﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JAlienInvasion.Controller;

namespace JAlienInvasion.Model
{
    class ShieldPowerUp : PowerUp
    {
		public ShieldPowerUp(double width, double height, double positionX, double positionY) : base(width, height, positionX, positionY)
		{
			
			try
			{
				this.Img = ScreenManager.GetImage(@"ShieldPowerUp.png");
			}
			catch (IOException e)
			{
				e.ToString();
			}
		}

		
		public override void UpgradePlayer()
		{
			Player.Instance.Shield = true;
		}
	}
}
