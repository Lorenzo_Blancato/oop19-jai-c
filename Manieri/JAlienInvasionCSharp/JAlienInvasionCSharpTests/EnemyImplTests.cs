﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using JAlienInvasion.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace JAlienInvasion.Model.Tests
{
    [TestClass()]
    public class EnemyImplTests
    {
        public EnemyImpl enemyInterceptor = new EnemyInterceptor(10, 10, 50, 10);
        public EnemyImpl enemyCruiser = new EnemyCruiser(10, 10, 50, 10);


        [TestMethod()]
        public void EnemyInterceptorTest()
        {
            Assert.IsNotNull(enemyInterceptor);
        }

        public void EnemyCruiserTest()
        {
            Assert.IsNotNull(enemyCruiser);
        }

        [TestMethod()]
        public void EnemyInterceptorIsAnEnemyTest()
        {
            Assert.IsInstanceOfType(enemyInterceptor, typeof(EnemyImpl));
        }

        public void EnemyCruiserIsAnEnemyTest()
        {
            Assert.IsInstanceOfType(enemyCruiser, typeof(EnemyImpl));
        }

        public void EnemyHealthTest()
        {
            double maxHealth = enemyCruiser.maxHealth;

            Assert.AreEqual(maxHealth, enemyCruiser.health);
        }

        public void EnemyDamageTest()
        {
            double hitHealth = 70;

            enemyCruiser.Hit(30);

            Assert.AreEqual(hitHealth, enemyCruiser.health);
        }


    }
}