﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAlienInvasion.Model
{
    public interface IEnemy
    {
		/// <summary>
		/// set enemy new position </summary>
		/// <returns> return true if an enemy is inside the screen else return false
		///  </returns>
		bool UpdateMovement();


		/// <summary>
		/// move enemy based on deltaX and deltaY </summary>
		/// <param name="deltaX"> amount used for move enemy coordinate x </param>
		/// <param name="deltaY"> amount used for move enemy coordinate y
		///  </param>
		void Move(int deltaX, int deltaY);

		/// <summary>
		/// check if enemy is inside screen </summary>
		/// <returns> return true if enemy is inside screen else false
		///  </returns>
		bool CheckBottomBorder();
	}
}
