﻿using JAlienInvasion.Controller;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace JAlienInvasion.Model
{
    public class EnemyInterceptor : EnemyImpl
    {
		private int randomBound = 100;
		private Random rand = new Random();

		public EnemyInterceptor(double width, double height, double positionX, double positionY) : base(width, height, positionX, positionY)
		{

			try
			{
				this.Img = ScreenManager.GetImage("EnemySpaceshipInterceptor.png");
			}
			catch (Exception)
			{
				Console.WriteLine("errore immagine");
			}
		}

		public override bool UpdateMovement()
		{
			int selector = rand.Next(randomBound);

			if (selector % 2 == 0)
			{
				MoveDown();
				MoveRight();

			}
			else
			{
				MoveDown();
				MoveLeft();
			}
			return this.CheckBottomBorder();

		}
	}
}
