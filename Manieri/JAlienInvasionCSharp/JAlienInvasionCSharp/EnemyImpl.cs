﻿using JAlienInvasion.Controller;

namespace JAlienInvasion.Model
{

	public abstract class EnemyImpl : LivingEntity, Enem
	{

		private HealthBarImpl healthBar;
		private const double ENEMY_INITIAL_DAMAGE = 25;

		public EnemyImpl(double width, double height, double positionX, double positionY) : base(width, height, positionX, positionY)
		{
			healthBar = new HealthBarImpl();
		}

		public void MoveUp()
		{
			Move(0, -1);
		}

		public void MoveDown()
		{
			Move(0, 1);
		}

		public void MoveRight()
		{
			Move(1, 0);
		}

		public void MoveLeft()
		{
			Move(-1, 0);
		}

		public override void Hit(double damage)
		{
			
			base.Hit(damage);
			this.healthBar.CurrentHealth = GetRelativeHealth();
			
		}

		public abstract bool UpdateMovement();


		public virtual void Move(int deltaX, int deltaY)
		{
			CheckMoveX(deltaX);			
			MoveY(deltaY);
		}

		public override void Draw()
		{
		}

		public virtual bool CheckBottomBorder()
		{
			if (this.Y > ScreenManager.HeightScreen)
			{
				//EnemyManager.removeEnemy(this);
				return false;
			}
			else
			{
				return true;
			}
		}


	}

}