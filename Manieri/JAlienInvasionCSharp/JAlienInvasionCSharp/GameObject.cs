﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;



namespace JAlienInvasion.Model
{

    public class GameObject : Entity
    {
        public const int DELTA_X = 1;
        public const int DELTA_Y = 1;
        private bool isVisible = true;
        private Image img; 

        public bool IsVisible { get { return isVisible; } set { isVisible = value; } }
        public Image Img { get { return img; } set { img = value; } }
        

        public GameObject(): base()
        {
        }

        public Rectangle GetBounds()
        {
            return new Rectangle(new Point((int)this.X, (int)this.Y), new Size((int)this.Width, (int)this.Height));
        }

        public bool CheckCollision(Rectangle r)
        {
            return this.GetBounds().IntersectsWith(r);
        }
        
        public virtual void Draw()
        {
            
        }
        
        public void Move(double deltaX, double deltaY)
        {
            MoveX(deltaX);
            MoveY(deltaY);
        }

        public void MoveX(double deltaX)
        {
            this.X += (deltaX*DELTA_X);
        }

        public void MoveY(double deltaY)
        {
            this.Y += (deltaY*DELTA_Y);
        }

    }
}
