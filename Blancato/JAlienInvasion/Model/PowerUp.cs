﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JAlienInvasion.Controller;
using JAlienInvasion.Model;

namespace JAlienInvasion.Model
{
    public abstract class PowerUp : GameObject, IPowerUp
    {
		private const double SPEED_Y = 0.5;

		public PowerUp(double width, double height, double positionX, double positionY):base(width, height, positionX, positionY)
		{
		}

		public bool UpdateMovement()
		{
			MoveY(DELTA_Y * SPEED_Y);
			if (this.Y > ScreenManager.HeightScreen)
			{
				return false;
			}
			else
			{
				return true;
			}
		}

		public abstract void UpgradePlayer();


	}
}
