﻿using JAlienInvasion.Controller;
using System.Drawing;
using System.Windows;

namespace JAlienInvasion.Model
{

	public class HealthBarImpl : IHealthBar { 

		private Rectangle maxHealthRect = new Rectangle();
		private Rectangle currentHealthRect = new Rectangle();
		private int width = 15;
		private int height = 5;



		public HealthBarImpl()
		{
			maxHealthRect = new Rectangle(new Point(),new Size(((int)ScreenManager.GetScaleX(width)), height));
			currentHealthRect = new Rectangle(new Point(), new Size(((int)ScreenManager.GetScaleX(width)), height));
		}

		public HealthBarImpl(int x, int y, int playerWidth, int playerHeight)
		{
			maxHealthRect = new Rectangle(new Point(x, y), new Size((int)(ScreenManager.GetScaleX(playerWidth)), (int)(ScreenManager.GetScaleY(playerHeight))));
			currentHealthRect = new Rectangle(new Point(x, y), new Size((int)(ScreenManager.GetScaleX(playerWidth)), (int)(ScreenManager.GetScaleY(playerHeight))));
		}

		public int CurrentHealth { get;  set; }

		double IHealthBar.HealthHeight { get => this.maxHealthRect.Height; }

		double IHealthBar.CurrentHealth { set => currentHealthRect.Width = maxHealthRect.Width * (int)value; }

		public void DrawRectangle(Graphics gc)
		{
			SolidBrush redBrush = new SolidBrush(Color.Red);
			gc.FillRectangle(redBrush, maxHealthRect);

			SolidBrush greenBrush = new SolidBrush(Color.LimeGreen);
			gc.FillRectangle(greenBrush, currentHealthRect);
		}
	}

}