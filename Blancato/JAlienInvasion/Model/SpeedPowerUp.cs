﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JAlienInvasion.Controller;

namespace JAlienInvasion.Model
{
    public class SpeedPowerUp : PowerUp
    {
		public SpeedPowerUp(double width, double height, double positionX, double positionY) : base(width, height, positionX, positionY)
		{
			try
			{
				this.Img = ScreenManager.GetImage(@"SpeedPowerUp.png");
			}
			catch (IOException e)
			{
				e.ToString();
			}
		}

		public override void UpgradePlayer()
		{
			Player.Instance.FireSpeed = (long)(Player.Instance.FireSpeed *  0.90);
		}

	}
}
