﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JAlienInvasion.Controller;

namespace JAlienInvasion.Model
{
    class HealthPowerUp : PowerUp
    {
		public HealthPowerUp(double width, double height, double positionX, double positionY) : base(width, height, positionX, positionY)
		{
			
			try
			{
				this.Img = ScreenManager.GetImage(@"HealthPowerUp.png");
			}
			catch (IOException e)
			{
				e.ToString();
			}

		}

			
		public override void UpgradePlayer()
		{
			Player.Instance.Health = Player.Instance.Health  * 1.25;
		}
	}
}
