﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JAlienInvasion.Model
{
    public interface IPowerUp
    {
        bool UpdateMovement();

        void UpgradePlayer();
    }
}
