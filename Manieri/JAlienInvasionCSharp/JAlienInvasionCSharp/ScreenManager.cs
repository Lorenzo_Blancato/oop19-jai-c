﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace JAlienInvasion.Controller
{
    class ScreenManager
    {
        public const int PREF_HEIGHT = 0;//valore preso dallo schermo che si usa
        public const int PREF_WIDTH = 0;
        public const int SCALE_X = 320;
        public const int SCALE_Y = 180;
        private static int heightScreen;
        private static int widthScreen;

       
        
        public static int HeightScreen { get { return heightScreen; } set { heightScreen = value; } }
        public static int WidthScreen { get { return widthScreen; } set { widthScreen = value; } }

    
        public static Image GetImage(String path)
        {
            //operazione di ricerca dell'immagine 
            Image tmp = new Bitmap(path);
            return tmp;
        }
       

        public static double GetScaleX(double deltaX)
        {
            return ((heightScreen * deltaX) / SCALE_Y);
        }

        public static double GetScaleY(double deltaY)
        {
            return ((widthScreen * deltaY) / SCALE_Y);
        }
    }
}