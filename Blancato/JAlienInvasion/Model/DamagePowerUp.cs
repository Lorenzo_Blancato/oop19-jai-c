﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JAlienInvasion.Controller;

namespace JAlienInvasion.Model
{
	public class DamagePowerUp : PowerUp
    {
		public  DamagePowerUp(double width, double height, double positionX, double positionY) : base(width, height, positionX, positionY)
		{
		
			try
			{
				this.Img = ScreenManager.GetImage(@"DamagePowerUp.png");
			}
			catch (IOException e)
			{
				e.ToString();
			}
		}

		
		public override void UpgradePlayer()
		{
			Player.Instance.Damage = Player.Instance.Damage * 1.10;
		}

	}
}

