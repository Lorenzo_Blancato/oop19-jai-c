﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using JAlienInvasion.Controller;

namespace JAlienInvasion.Model
{
    public class LivingEntity : BorderGameObject
    {
		private const String EXPLOSION1 = @"Explosion1.png";
		private const String EXPLOSION2 = @"Explosion2.png";
		private const String EXPLOSION3 = @"Explosion3.png";
		private const double INITIAL_HEALTH = 100;
		private const long INITIAL_FIRE_SPEED = 20;

		public double health = INITIAL_HEALTH;
		public double maxHealth = INITIAL_HEALTH;
		private int explosionStep = 0;
		private bool exploding = false;
		private bool removable = false;
		private long fireSpeed = INITIAL_FIRE_SPEED; 
		private double damage;


		public long FireSpeed { get { return this.fireSpeed; } set { this.fireSpeed = value; } }
		public bool Removable { get { return this.removable; } set { this.removable = value; } }
		public double MaxHealth { get { return this.maxHealth; } set { this.maxHealth = value; } }
		public double Damage { get { return this.damage; } set { this.damage = value; } }
		public bool Exploding { get { return this.exploding; } set { this.exploding = value; } }
		
		public double Health
		{
			get { return this.health; }
			set
			{
				if (value > this.maxHealth)
				{
					this.health = value;
					this.maxHealth = this.health;
				}
				else
				{
					this.health = value;
				}
			}
		}

		public LivingEntity(double width, double height, double positionX, double positionY):base(width, height, positionX, positionY)
		{
		
		}

		public virtual void Hit(double damage)
		{
			this.Health = this.Health - damage;
			if (this.Health <= 0.0)
			{
				this.Exploding = true;
			}
		}

		


		public void CheckExplode()
		{
			if (this.Exploding)
			{
				this.Explode();
			}
		}


		//passare canvas per argomento 
		public override void  Draw()
		{
			CheckExplode();
			base.Draw();
		}

		public void Explode()
		{
			if (this.exploding)
			{
				if (explosionStep == 0)
				{
				//	SoundManager.playExplosionClip();
				}
				explosionStep++;
				try
				{
					if (explosionStep <= 20)
					{
						this.Img = ScreenManager.GetImage(EXPLOSION1);
					}
					if (explosionStep > 20 && explosionStep <= 40)
					{
						this.Img = ScreenManager.GetImage(EXPLOSION2);
					}
					if (explosionStep > 40 && explosionStep <= 60)
					{
						this.Img = ScreenManager.GetImage(EXPLOSION3);
					}
					if (explosionStep > 60)
					{
						explosionStep = 0;
						this.Removable = true;
						this.IsVisible = false;
					}
				}
				catch (IOException e)
				{
					e.ToString();
				}
			}
		}

		public double GetRelativeHealth()
		{
			return this.health / this.maxHealth;
		}

		public virtual void Firing()
		{
			//SoundManager.playFireClip();
		}


	}
}
