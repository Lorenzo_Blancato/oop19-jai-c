﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using JAlienInvasion.Model;
using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework.Internal;

namespace JAlienInvasion.Model.Tests
{
    [TestClass()]
    public class HealthBarImplTests
    {
        public HealthBarImpl healthBar = new HealthBarImpl();
        public HealthBarImpl healthBarInitilized = new HealthBarImpl(10, 10, 50, 15);

        [TestMethod()]
        public void HealthBarImplTest()
        {
            Assert.IsNotNull(healthBar);
        }

        [TestMethod()]
        public void HealthBarImplInitializedTest()
        {
            Assert.IsNotNull(healthBarInitilized);
        }

    }
}