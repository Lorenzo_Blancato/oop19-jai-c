﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JAlienInvasion.Model;

namespace JAlienInvasion.Controller
{
    public class PowerUpManager
    {
		private const int POWER_UP_WIDTH = 10;
		private const int POWER_UP_HEIGHT = 10;
		private const int SPAWN_POWER_UP = 10;
		private static int lastPowerUp = 0;

		private static List<PowerUp> powerUpManager = null;
		public static List<PowerUp> GetList { 
			get
            {
				if (powerUpManager == null)
				{
					powerUpManager = new List<PowerUp>();
				}
				return powerUpManager;
			} }

	
		public void AddPowerUp(PowerUp p)
		{
			PowerUpManager.GetList.Add(p);

		}

		
		public void RemovePowerUp(PowerUp p)
		{
			PowerUpManager.GetList.Remove(p);
		}

	
		public void Update()
		{
			if (PowerUpManager.GetList.Count > 0)
			{
				foreach (PowerUp p in PowerUpManager.GetList)
				{
					if (p.UpdateMovement())
					{
						p.Draw();
						if (p.CheckCollision(Player.Instance.GetBounds()))
						{
							p.UpgradePlayer();
							this.RemovePowerUp(p);
						}
					}
					else
					{
						this.RemovePowerUp(p);
					}
				}
			}
			else
			{
				/*
				if (EnemyManager.enemyKilled % SPAWN_POWER_UP == 0 && EnemyManager.enemyKilled > 0 && EnemyManager.enemyKilled > lastPowerUp)
				{
					var tmpX, tmpY;
					tmpX = EnemyManager.lastEnemy.getPositionX();
					tmpY = EnemyManager.lastEnemy.getPositionY();
					this.spawnPowerUp(tmpX, tmpY);
					lastPowerUp = EnemyManager.enemyKilled;
				}
				*/
			}
		}

		private void SpawnPowerUp(double x, double y)
		{
			Random rand = new Random();
			int tmp = rand.Next(100);
			if (Player.Instance.FireSpeed > 1)
			{
				tmp = tmp % 4;
			}
			else
			{
				tmp = tmp % 3;
			}

			switch (tmp)
			{

				case 0:
					PowerUp p3 = new HealthPowerUp(POWER_UP_WIDTH, POWER_UP_HEIGHT, x, y);
					this.AddPowerUp(p3);
					break;

				case 1:
					PowerUp p1 = new DamagePowerUp(POWER_UP_WIDTH, POWER_UP_HEIGHT, x, y);
					this.AddPowerUp(p1);
					break;

				case 2:
					PowerUp p2 = new ShieldPowerUp(POWER_UP_WIDTH, POWER_UP_HEIGHT, x, y);
					this.AddPowerUp(p2);
					break;
				case 3:
					PowerUp p = new SpeedPowerUp(POWER_UP_WIDTH, POWER_UP_HEIGHT, x, y);
					this.AddPowerUp(p);
					break;

				default: break;
			}
		}

		/**
		 * Clear the powerUp manager list
		 */
		public static void ResetPowerUpManager()
		{
			powerUpManager.Clear();
		}
	}
}
