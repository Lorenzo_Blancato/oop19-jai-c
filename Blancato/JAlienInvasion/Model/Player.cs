﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using JAlienInvasion.Controller;
using JAlienInvasion.Model;



namespace JAlienInvasion.Model
{
    public sealed class Player : LivingEntity, IPlayer
    {
		private const String PLAYER_BASIC_IMAGE = @"Player.png";
		private const String PLAYER_SHIELD_IMAGE = @"PlayerWithShield.png";

		private const double WIDTH = 15;
		private const double HEIGHT = 15;
		private static readonly double POSITION_X =  ScreenManager.GetScaleX(ScreenManager.SCALE_X/ 2);
		private static readonly double POSITION_Y = ScreenManager.GetScaleY(ScreenManager.SCALE_Y - 30);
		private const double SPEED = 1;
		private const double INITIAL_DAMAGE = 50;
		private const double PLAYER_COLLISION_DAMAGE = 100;
		private const double INITIAL_HEALTH = 100;

		//private TimeManager playerManager;
		private bool blinking = false;
		private bool shield = false;
		private bool canFire = true;
		private bool up = false;
		private bool down = false;
		private bool right = false;
		private bool left = false;
		private bool fire = false;

		public bool Blinking { get { return this.blinking; } set { this.blinking = value; } }
		
		public bool CanFire { get { return this.canFire; }	set { this.canFire = value; } }
		public bool Up { get { return this.up; } set { this.up = value; } }
		public bool Down { get { return this.down; } set { this.down = value;} }
		public bool Right {	get { return this.right; } set { this.right = value; } }
		public bool Left { get { return this.left; } set { this.left = value; } }
		public bool Fire { get { return this.fire; } set { this.fire = value; } }
		public bool Shield
		{
			get { return this.shield; }
			set
			{
				this.shield = value;
				if (this.shield)
				{
					try
					{
						this.Img = ScreenManager.GetImage(PLAYER_SHIELD_IMAGE);
					}
					catch (IOException e)
					{
						e.ToString();
					}
					//this.playerManager.setShieldTimer();
				}
				else
				{
					try
					{
						this.Img = ScreenManager.GetImage(PLAYER_BASIC_IMAGE);
					}
					catch (IOException e)
					{
						e.ToString();
					}
					//this.playerManager.resetShield();
				}
			}
		}


		private static Player instance;
		public static Player Instance
		{
			get
			{
				if (Player.instance == null)
				{
					Player.instance = new Player(WIDTH, HEIGHT, POSITION_X, POSITION_Y);
					return Player.instance;
				}
				return Player.instance;
			}
		}

		private Player(double width, double height, double positionX, double positionY): base(width, height, positionX, positionY)
		{

			this.Health = INITIAL_HEALTH;
			this.Damage = INITIAL_DAMAGE;
			try
			{
				this.Img = ScreenManager.GetImage(PLAYER_BASIC_IMAGE);
			}
			catch (IOException e)
			{
				this.IsVisible = true;
				Console.WriteLine("errore immagine");
				e.ToString();
			}

			//this.playerManager = TimeManagerImpl.getInstance();
			//this.playerManager.setFireTimer(this.getFireSpeed());
		}

		

		
	



		
		public override void Draw()
		{
			if (!this.Removable)
			{
				this.UpdateDirection();
				base.Draw();
			}
			else
			{
				/*
				ScoreManager sm = ScoreManagerImpl.getInstance();
				ScoreManagerImpl.getListFromFile();
				sm.addPersonalScore(sm.getCurrentScore());
				ScoreManagerImpl.sortList();
				ScoreManagerImpl.writeListToFile();
				GameOver.gameOver(Game.gc);
				*/
			}
		}

		

		
		






		
		public override void Move(double deltaX, double deltaY)
		{
			CheckMoveX(deltaX * SPEED);
			CheckMoveY(deltaY * SPEED);
		}

		
		public override void Firing()
		{
			if (this.CanFire)
			{
				base.Firing();
				/*
				RedBullet b = new RedBullet(1, 3,
						this.getPositionX() + (this.width / 2), this.getPositionY(),
						Target.ENEMY, this.getDamage());
				BulletManager.addBullet(b);
				*/
				this.CanFire = false;
			}
		}



			
		public override void Hit(double damage)
		{
			base.Hit(damage);
			if (!this.Exploding)
			{
				this.Blinking = true;
				//playerManager.setBlinkingTimer();
			}


		}



		
		public void MoveUp()
		{
			this.Move(0, -DELTA_Y);
		}



		
		public void MoveDown()
		{
			this.Move(0, DELTA_Y);
		}



		
		public void MoveLeft()
		{
			this.Move(-DELTA_X, 0);
		}



		public void MoveRight()
		{
			this.Move(DELTA_X, 0);
		}

		public void SetDirection(Direction d)
		{
			switch (d)
			{
				case Direction.UP:
					this.up = true;
					break;
				case Direction.DOWN:
					this.down = true;
					break;
				case Direction.LEFT:
					this.left = true;
					break;
				case Direction.RIGHT:
					this.right = true;
					break;
				case Direction.SPACE: 
					this.fire = true;
					break;
				default:
					break;
			}
		}

		public void DeleteDirection(Direction d)
		{
			switch (d)
			{
				case Direction.UP:
					this.up = false;
					break;
				case Direction.DOWN:
					this.down = false;
					break;
				case Direction.LEFT:
					this.left = false;
					break;
				case Direction.RIGHT:
					this.right = false;
					break;
				case Direction.SPACE:
					this.fire = false;
					break;
				default:
					break;
			}
		}

		//Metodo per effettuare i movimenti in base alle variabili di direzione impostate
		private void UpdateDirection()
		{
			if (Up)
				this.MoveUp();
			if (Down)
				this.MoveDown();
			if (Left)
				this.MoveLeft();
			if (Right)
				this.MoveRight();
			if (Fire)
				this.Firing();
		}


		public static void ResetPlayer()
		{

			Player.instance = null; 
			Player.instance = Player.Instance;
		
		}


	}
}
