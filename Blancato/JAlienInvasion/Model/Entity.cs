﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public abstract class Entity
    {
        private double width;
        private double height;
        private double x;
        private double y;

       
        public double Width { get { return width; } set { width = value; } }
        public double Height { get { return height; } set { height = value; } }

        public double X { get { return x; } set { x = value; } }

        public double Y { get { return y; } set { y = value; } }
        public Entity(double width, double height, double x, double y)
        {
            this.Width = width;
            this.Height = height;
            this.X = x;
            this.Y = y;
        }

        protected Entity()
        {
        }
    }
}
