﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JAlienInvasion.Controller;

namespace JAlienInvasion.Model
{
	public abstract class Bullet : GameObject, IBullet
	{
		private const double SPEED = 1.2;
		private Target target;
		private double damage;

		public Target BulletTarget { get { return this.target; } set { this.target = value; } }
		public double Damage { get { return this.damage; } set { this.damage = value; } }

		/**
		 * 
		 * @param x
		 * @param y
		 * @param width
		 * @param height
		 * @param target
		 * @param damage
		 */
		public Bullet(double width, double height, double x, double y, Target target, double damage) : base(width, height, x, y)
		{
			this.damage = damage;
			this.target = target;
		}






		public bool UpdateMovement()
		{
			if (this.Y > ScreenManager.HeightScreen || this.Y + this.Height < 0)
			{
				this.IsVisible = false;
				return false;
			}
			else
			{
				if (this.target == Target.ENEMY)
				{
					this.MoveY(-DELTA_Y * SPEED);
				}
				else
				{
					this.MoveY(DELTA_Y * SPEED);
				}
				return true;
			}

		}
	}


	public enum Target
	{
		ENEMY,
		PLAYER
	}

}
