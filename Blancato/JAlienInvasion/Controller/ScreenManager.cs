﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace JAlienInvasion.Controller
{
    public class ScreenManager
    {

        public const int SCALE_X = 320;
        public const int SCALE_Y = 180;
        private static int heightScreen;
        private static int widthScreen;

       
        
        public static int HeightScreen { get { return heightScreen; } set { heightScreen = value; } }
        public static int WidthScreen { get { return widthScreen; } set { widthScreen = value; } }

        public void SetUpScreenManager()
        {
            
            HeightScreen = (Screen.PrimaryScreen.Bounds.Height * 9)/10;
            WidthScreen = (Screen.PrimaryScreen.Bounds.Width * 9) / 10;

        }
        
        public static Image GetImage(String path)
        {
            //operazione di ricerca dell'immagine 
            try
            {
               return Image.FromFile(Path.GetFileName(path));

            }
            catch (IOException e)
            {
                e.ToString();
            }
            return null;
        }
        
        public static double GetScaleX(double deltaX)
        {
            return ((heightScreen * deltaX) / SCALE_Y);
        }

        public static double GetScaleY(double deltaY)
        {
            return ((widthScreen * deltaY) / SCALE_Y);
        }
    }
}