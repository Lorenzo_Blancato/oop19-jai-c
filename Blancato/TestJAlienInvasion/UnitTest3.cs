﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Drawing;
using JAlienInvasion.Controller;

namespace TestJAlienInvasion
{
    [TestClass]
    public class UnitTest3
    {

        /*
         * Nota: le immagini vengono trovate perchè le ho impostate in modo tale
         * che vengano copiate nella cartella di runtime quando vengono eseguite.
         * Questo perchè la cartella principale dove si vanno a cercare durante l'esecuzione
         * è: *\bin\debug
         * */


        [TestMethod]
        public void TestScreenManager()
        {
            
            Image image = ScreenManager.GetImage(@"\Player.png");
            Assert.IsNotNull(image);
        }
    }
}
